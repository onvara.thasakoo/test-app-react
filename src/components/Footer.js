import React from 'react'

const Footer = () => {
    return (
        <>
            <div className="container">
                <hr />
                <p>Footer &copy; {new Date().getFullYear()} </p>
            </div>

        </>
    )
}

export default Footer
