import React, { Component } from 'react';
import axios from 'axios';
import '../components/style.css';
import Welcome from '../components/Welcome';
import { Link } from 'react-router-dom';

export default class home extends Component {
    state = {
        Myname: 'MILD',
        data: [],
    }
    
    // getUsers = async () => {
    //     let response = await axios.get("https://shop-backendapi.herokuapp.com/api/shop");
    //     console.log(response.data)
    //     this.setState({ 

    //     });
    // };

    async componentDidMount() {
        const response = await axios.get("https://shop-backendapi.herokuapp.com/api/shop");
        const json = response.data.data;

        console.log(json)
        this.setState({ data: json, });
        // console.log(data);
        // const {data:{name,photo,location:{lat,lgn}}} = response.data;
        // this.setState({ 
        //     name: name,
        //     photo: photo
        // });
    }
    // array.map((myarray,index) =>{
    //     console.log(`${index}) ${myarray}`)
    // })
    render() {
        return (
            <div class="container my-5">
                <Welcome name={this.state.Myname}/>
                    <div class="card-deck mb-3 set-center">
                    {this.state.data.map((el,i) => (
                        // <div class="col-md-4 d-flex ">
                        <div class="card-group">
                            <div className="card mb-3 border-info" style={{ width: '18rem' }}>
                                <img className="card-img-top size-image p-2" src={el.photo} alt="Card image cap"/>
                                <div className="card-body text-dark">
                                    <h5 className="card-title">{el.name}</h5>
                                    <Link to={`/menu/${el.id}`} className="nav-link" >Menu</Link>
                                    {/* <a href={`/menu/${el.id}`} className="btn btn-link" >Menu</a> */}
                                     <a href={`https://www.google.com/search?q=${el.location.lat}+${el.location.lgn}`} className="btn btn-link" >see location ></a>
                               </div>
                            </div>
                         </div>
                    ))}
    </div>
                </div>
// </div>

        );
    }
}