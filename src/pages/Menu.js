import React, { Component } from 'react'
import axios from 'axios';

export default class Menu extends Component {
    state = {
        menu: [],
        shop: {}
    }
    async componentDidMount() {
        const id = this.props.match.params.id // we grab the ID from the URL
        console.log(id);
        const res = await axios.get(`https://shop-backendapi.herokuapp.com/api/shop/${id}`)
        const json = res.data.data;
        const json2 = json.menus;
        console.log(json)
        console.log(json2)
        this.setState({ menu: json2, shop: json })
        console.log(this.state.menu)

    }

    render() {
        return (
            <div className="container my-3">

                <h1>Menu of  {this.state.shop.name}</h1>
                <ul className="list-group">
                    {this.state.menu.map((em, i) => (
                        <li className="list-group-item d-flex justify-content-between align-items-center">
                            {em.name}
                            <span className="badge badge-primary badge-pill ">{em.price.$numberDecimal} Bath</span>
                          
                        </li>
                    ))} </ul>
            </div>

        )
    }
}
